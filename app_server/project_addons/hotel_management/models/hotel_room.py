from operator import mod
from odoo import _, api, fields, models


class HotelRoom(models.Model):
    """
    Hotel Room
    """
    _name = "hotel.room"
    _description = "Hotel Room"

    name = fields.Char(name="Name", copy=False)
    hotel_id = fields.Many2one(comodel_name="hotel.hotel", name="Hotel", required=True)
    type_id = fields.Many2one(comodel_name="hotel.room.type", name="Type")
    floor_number = fields.Integer("Floor Number")
