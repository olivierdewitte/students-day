from odoo import _, api, fields, models


class HotelRoomType(models.Model):
    """
    Hotel Room Type
    """
    _name = "hotel.room.type"
    _description = "Hotel Room Type"

    name = fields.Char("Name", required=True)
    