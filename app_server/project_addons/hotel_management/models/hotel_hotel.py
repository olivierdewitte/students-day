from odoo import _, api, fields, models


class HotelHotel(models.Model):
    """
    Hotel Class
    """
    _name = "hotel.hotel"

    name = fields.Char("Name", required=True, copy=False, description="The name of the hotel")
    address_id = fields.Many2one("res.partner", "Address")

    room_ids = fields.One2many("hotel.room", "hotel_id", "Rooms")
    room_count = fields.Integer(compute="_compute_room_count")

