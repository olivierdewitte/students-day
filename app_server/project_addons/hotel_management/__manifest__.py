# -*- coding: utf-8 -*-
{
    'name': "hotel_management",
    'summary': """Hotel Management Module""",
    'description': """Hotel Management""",
    'author': "Idealis Consulting SPRL",
    'website': "http://www.idealisconsulting.com",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}
